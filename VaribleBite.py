"""
Variables: 
Variables are used to store data, like string, numbers, date-time etc.
When you create a variable, the interpreter will reserve some space in the memory to store value of variable.
Value of a variable can change during the execution of a python program.

"""

import sys

#Example 1
print("# Example 1")
A = 12 # Integer varible
B = "This is string varible" #string varible
print(A)
print(B)

#Example 2
print("# Example 2")
if type(A) == int:
	print("Integer")
print(type(B))

#Example 3
print("# Example 3")
num = 10
if 'num' in locals(): # locals() usually used to check the existence of a local variable.
	print("Varible num exists")