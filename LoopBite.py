"""
Loop:
loop causes a block of code to repeat as long as a condition is true
"""

"""
### While Loop ###
"""
#Example 1
print("# Example 1")

num = 0
while  num <=10:
	print("2 * ", num, " = ", 2 * num)
	num = num + 1


#Example 2
print("# Example 2")

num = 0
while  num <=10:
	print("2 * ", num, " = ", 2 * num)
	if num == 5:
		break
	num = num + 1

#Example 3
print("# Example 3")

num = 0
while  num <=10:
	num = num + 1
	if num >= 5:
		continue
	print("2 * ", num, " = ", 2 * num)

"""
### For Loop ###
"""
#Example 1
print("# Example 1")
for i in [1,2,3,4,5]:
    print("For loop")

"""
range() function: The range() function creates a type of object known as an iterable to simplifies the process 
of writing a count-controlled for loop. The range() function returns a sequence of numbers, which starts from 0,
increase by 1 each time the loop runs and ends at a specified number.
"""
#Example 2
print("# Example 2")
for i in range(5):
    print("range function")

#Example 3
print("# Example 3")
for i in range(5, 10):
    print("itreation i: ", i)

#Example 4
print("# Example 4")
for i in range(1, 10, 2):
    print(i)

#Example 5
print("# Example 5")
newlist = ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]
for item in newlist:
    print(item)

#Example 6
print("# Example 6")
newdict = {'Mon':100, 'Tue':200, 'Wed':300, 'Thur':400, 'Fri':500, 'Sat':600, 'Sun':700}
for item in newdict:
    print(item)

#Example 7
print("# Example 7")
newtupple = ("Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun")
for item in newtupple:
    print(item)
