"""
If…Else Statements:
The if statement is used to write decision-making code,
which allows a program to have more than one path of execution.
"""

#Example 1
print("# Example 1")

A = 10
B = 15

if A < B:
	print("A is lessthan B")

#Example 2
print("# Example 2")

A = 10
B = 8
if A < B:
	print("A is less than B")
else:
	print("B is less than A")

#Example 3
print("# Example 3")

A = 15
B = 15
C = 20
if A < B:
	print("A is less than B")
elif A < C:
	print("A is less than C")
else:
	print("A is equal to B")

#Example 4
print("# Example 4")

A = 10
B = 5

if(A > B): print("A")
print("A") if A > B else print("B")
print("A") if A > B else print("A == B") if A == B else print("B")


